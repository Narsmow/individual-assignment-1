The reason that a client for a capstone project would prefer you to use a private repository over a public one is because they may want to make the code/software
that you are creating for them proprietary and do not want to leak any of the source code to the public as well as this allows for more security of the code and other 
items created for that client. if the client was working on a project that was going to be open source then most often they will have a public repositry with a licence
of some sort stating the use case of the software in the repository.