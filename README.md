  
 Copyright [2020] [Chris Freriks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


Instructions:

1. Copy the command "git clone https://Narsmow@bitbucket.org/Narsmow/individual-assignment-1.git"
2. Paste the command into your git bash Console and press enter in the Designated location you would like to add the folder from
3. Open up CFreriksAssignment3->CFreriksAssignment3->Bin->Debug->CFreriksAssignment3.exe this will launch the application
    -The goal of the application is to have a hot seat experience while playing tic tac toe.   
        0 The first user Starts as an X then alternates with another player untill there are no more posistions on the board or one of the players win
        0 The game will automaticaly reset after this point 

The reason I chose this license is because it is open source and anyone can use it for any purpose they feel fit.